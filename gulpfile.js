"use strict";

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    del = require('del'),
    uglify = require('gulp-uglify'),
    cleanCSS = require('gulp-clean-css'),
    rename = require("gulp-rename"),
    // merge = require('merge-stream'),
    htmlreplace = require('gulp-html-replace'),
    autoprefixer = require('gulp-autoprefixer');

var browserSync = require('browser-sync').create();

// Clean task
gulp.task('clean', function() {
  return del(['dist', './src/assets/css/*.css']);
});

gulp.task('scss', gulp.series(function compileScss() {
  return gulp.src(['./src/assets/scss/*.scss'])
    .pipe(sass.sync({
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(gulp.dest('./src/assets/css'))
}));

gulp.task('css:minify', gulp.series('scss', function cssMinify() {
  return gulp.src("./src/assets/css/*.css")
    .pipe(cleanCSS())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('./dist/assets/css'))
    .pipe(browserSync.stream());
}));

gulp.task('js:minify', function () {
  return gulp.src([
    './src/assets/js/app.js'
  ])
    .pipe(uglify())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest('./dist/assets/js'))
    .pipe(browserSync.stream());
});

gulp.task('replaceHtmlBlock', function () {
  return gulp.src(['src/*.html'])
    .pipe(htmlreplace({
      'js': 'assets/js/app.min.js',
      'css': 'assets/css/app.min.css'
    }))
    .pipe(gulp.dest('dist/'));
});


// Dev
gulp.task('dev', function browserDev(done) {
  browserSync.init({
    server: {
      baseDir: "./src"
    }
  });
  gulp.watch(['src/assets/scss/*.scss','src/assets/scss/**/*.scss','!src/assets/scss/bootstrap/**'], gulp.series('css:minify', function cssBrowserReload (done) {
    browserSync.reload();
    done();
  }));
  gulp.watch('src/assets/js/app.js', gulp.series('js:minify', function jsBrowserReload (done) {
    browserSync.reload();
    done();
  }));
  gulp.watch(['src/*.html']).on('change', browserSync.reload);
  done();
});

// Build
gulp.task("build", gulp.series(gulp.parallel('css:minify', 'js:minify'), function copyAssets() {
  return gulp.src([
    'src/*.html',
    'favicon.ico',
    "assets/img/**"
  ], { base: './'})
    .pipe(gulp.dest('dist'));
}));

// Default
gulp.task("default", gulp.series("clean", 'build', 'replaceHtmlBlock'));