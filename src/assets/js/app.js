$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar, #content').toggleClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        $(this).toggleClass('active');
    });
    // $('.banner').height(function(){
    //     return $(this).width() * 0.374;
    // })

    $('.color-container').each(function(i) {
        hex = $(this).data('hex');
        rgb = $(this).data('rgb');
        $(this).append(
            '<div class="color" style="background-color:' + hex + '"></div>' +
            '<div class="color-caption"><p><strong>HEX:</strong> ' + hex + '</p><p><strong>RGB:</strong> ' + rgb + '</p>'
            );
    });
    $('a.dropdown-toggle.active + ul.collapse').collapse("toggle");
});